from __future__ import division
import random
import re


def load_deck(path):
    with open(path) as f:
        lines = f.read().split('\n')
    name_num_re = '(?<=^\d)\s*[ x]\s*'
    deck = [re.split(name_num_re, line) for line in lines]
    deck = {l[1]: int(l[0]) for l in deck}
    ret = []
    for k in deck:
        ret.extend([k] * deck[k])
    return ret


def hand_has_combo(hand):
    if(hand["toad"] >= 1 and hand["enchantment"] >= 1):
        if(hand["dance"] >= 2 and hand["piper"] + hand["dance"] >= 4):
            return True
    return False


def simulate(num_runs):
    d = load_deck("SuperToad.deck")
    count = 0
    cards = set(d)
    for k in xrange(num_runs):
        #simulate a run
        deck = d[:]
        random.shuffle(deck)
        hand = {card: 0 for card in cards}
        num_turns = 6
        for x in xrange(3 + num_turns):
            hand[deck.pop()] += 1
        mana = num_turns * 3
        while(True):
            try:
                # win condition
                if(mana >= 18 and hand_has_combo(hand)):
                    count += 1
                    break
                # Windfall doesn't cost anything. Always do first if possible           
                elif(mana >= 9 and hand["windfall"] >= 1):
                    hand["windfall"] -= 1
                    mana += 3
                # Spawn+Feed is next strongest combo
                elif(mana >= 11 and hand["spawn"] >= 1 and hand["feed"] >= 1):
                    mana += 4
                    hand["spawn"] -= 1
                    hand["feed"] -= 1
                    hand[deck.pop()] += 1
                    hand[deck.pop()] += 1            
                elif(hand["tides"] >= 1 and mana >= 1):
                    hand["tides"] -= 1
                    hand[deck.pop()] += 1
                    mana -= 1
                elif(hand["wisdom"] >= 1 and mana >= 3):
                    hand["wisdom"] -= 1
                    hand[deck.pop()] += 1
                    hand[deck.pop()] += 1
                    mana -= 3
                elif(hand["earthcraft"] >= 1 and mana >= 2):
                    hand["earthcraft"] -= 1
                    hand[deck.pop()] += 1
                    mana -= 2
                else:
                    break
            except:
                break
    print(count/num_runs)



if(__name__ == "__main__"):
    simulate(10000)

